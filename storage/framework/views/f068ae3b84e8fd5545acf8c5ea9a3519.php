<?php $__env->startSection('content'); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid my-2">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit Sub Category</h1>
            </div>
            <div class="col-sm-6 text-right">
                <a href="<?php echo e(route('sub-categories.index')); ?>" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="container-fluid">
        <form action=""  name="subCategoryForm" id="subCategoryForm">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="name">Category</label>
                                <select name="category" id="category" class="form-control">
                                    <option value="">Select a category</option>
                                    <?php if($categories->isNotEmpty()): ?>
                                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option <?php echo e(($subCategory->category_id == $category->id) ? 'selected' : ''); ?> value='<?php echo e($category->id); ?>'><?php echo e($category->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </select>
                                <p></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Name" value="<?php echo e($subCategory->name); ?>">
                                <p></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label  for="slug">Slug</label>
                                <input type="text" name="slug" id="slug" readonly class="form-control" placeholder="Slug" value="<?php echo e($subCategory->slug); ?>">
                                <p></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="status">Status</label>
                                    <select name='status' id='status' class="form-control">
                                        <option value="">Select a status</option>
                                        <option <?php echo e(($subCategory->status == 1) ? 'selected' : ''); ?> value="1">Active</option>
                                        <option <?php echo e(($subCategory->status == 2) ? 'selected' : ''); ?> value="2">Block</option>
                                    </select>
                                    <p></p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="pb-5 pt-3">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="<?php echo e(route('sub-categories.index')); ?>" class="btn btn-outline-dark ml-3">Cancel</a>
            </div>
        </form>
    </div>
    <!-- /.card -->
</section>
<!-- /.content -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customJs'); ?>
<script>
$("#subCategoryForm").submit(function(event){
    event.preventDefault();

    var element = $('#subCategoryForm');

    $('button[type=submit]').prop('disabled', true);

    $.ajax({
        url: '<?php echo e(route("sub-categories.update", $subCategory->id)); ?>',
        type: 'put',
        data: element.serializeArray(),
        dataType: 'json',
        success: function(response){
            $('button[type=submit]').prop('disabled', false);
            if(response['status'] == true) {
                    window.location.href = '<?php echo e(route('sub-categories.index')); ?>';

                     $('#name').removeClass('is-invalid')
                    .siblings('p')
                    .removeClass('invalid-feedback').html('');

                    $('#slug').removeClass('is-invalid')
                    .siblings('p')
                    .removeClass('invalid-feedback').html('');

                    $('#category').removeClass('is-invalid')
                    .siblings('p')
                    .removeClass('invalid-feedback').html('')

                    $('#status').removeClass('is-invalid')
                    .siblings('p')
                    .removeClass('invalid-feedback').html('')


            } else {

                if(respomse['notFound'] == true) {
                    window.location.href = '<?php echo e(route('sub-categories.index')); ?>';
                    return false;
                }

                var errors = response['errors'];
                if(errors['name']) {
                    $('#name').addClass('is-invalid')
                    .siblings('p')
                    .addClass('invalid-feedback').html(errors['name']);
                } else {
                    $('#name').removeClass('is-invalid')
                    .siblings('p')
                    .removeClass('invalid-feedback').html('');
                }

                if(errors['slug']) {
                    $('#slug').addClass('is-invalid')
                    .siblings('p')
                    .addClass('invalid-feedback').html(errors['slug'])
                } else {
                     $('#slug').removeClass('is-invalid')
                    .siblings('p')
                    .removeClass('invalid-feedback').html('');
                }

                if(errors['category']) {
                    $('#category').addClass('is-invalid')
                    .siblings('p')
                    .addClass('invalid-feedback').html(errors['category'])
                } else {
                     $('#category').removeClass('is-invalid')
                    .siblings('p')
                    .removeClass('invalid-feedback').html('');
                }

                if(errors['status']) {
                    $('#status').addClass('is-invalid')
                    .siblings('p')
                    .addClass('invalid-feedback').html(errors['status'])
                } else {
                     $('#status').removeClass('is-invalid')
                    .siblings('p')
                    .removeClass('invalid-feedback').html('');
                }
            }
        },
        error: function(jqXHR, exception ){
            console.log('Something went wrong');
        }
    })
});

$('#name').change(function(){
    element = $(this);
    $('button[type=submit]').prop('disabled', true);

    $.ajax({
        url: '<?php echo e(route("getSlug")); ?>',
        type: 'get',
        data: {title: element.val()},
        dataType: 'json',
        success: function(response){
            $('button[type=submit]').prop('disabled', false);
            if(response['status'] == true){
                $('#slug').val(response['slug']);
            }
        },
    });
});
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/egordultsev/MY/work/laravel/laravel-online-shop/resources/views/admin/sub_category/edit.blade.php ENDPATH**/ ?>