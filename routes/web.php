<?php

use App\Http\Controllers\admin\AdminLoginController;
use App\Http\Controllers\admin\AdminHomeController;
use App\Http\Controllers\admin\AdminCategoryController;
use App\Http\Controllers\admin\AdminSubCategoryController;
use App\Http\Controllers\admin\AdminTempImagesController;
use App\Http\Controllers\admin\AdminBrandsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::group(['prefix' => 'admin'], function () {

    Route::group(['middleware' => 'admin.guest'], function () {
        Route::get('/login', [AdminLoginController::class, 'index']) ->name('admin.login');
        Route::post('/authenticate', [AdminLoginController::class, 'authenticate']) ->name('admin.authenticate');

    });

    Route::group(['middleware' => 'admin.auth'], function () {
        Route::get('/dashboard', [AdminHomeController::class, 'index'])->name('admin.dashboard');
        Route::get('/logout', [AdminHomeController::class, 'logout'])->name('admin.logout');

        //Categories Routes
        Route::get('/categories', [AdminCategoryController::class, 'index'])->name('categories.index');
        Route::get('/categories/create', [AdminCategoryController::class, 'create'])->name('categories.create');
        Route::post('/categories', [AdminCategoryController::class, 'store'])->name('categories.store');
        Route::post('/upload-temp-image', [AdminTempImagesController::class, 'create'])->name('temp-images.create');
        Route::get('/categories/{category}/edit', [AdminCategoryController::class, 'edit'])->name('categories.edit');
        Route::put('/categories/{category}', [AdminCategoryController::class, 'update'])->name('categories.update');
        Route::delete('/categories/{category}', [AdminCategoryController::class, 'destroy'])->name('categories.delete');

        //Subcategory Routes
        Route::get('/sub-categories/create', [AdminSubCategoryController::class, 'create'])->name('sub-categories.create');
        Route::post('/sub-categories', [AdminSubCategoryController::class, 'store'])->name('sub-categories.store');
        Route::get('/sub-categories', [AdminSubCategoryController::class, 'index'])->name('sub-categories.index');
        Route::get('/sub-categories/{subCategory}/edit', [AdminSubCategoryController::class, 'edit'])->name('sub-categories.edit');
        Route::put('/sub-categories/{subCategory}', [AdminSubCategoryController::class, 'update'])->name('sub-categories.update');
        Route::delete('/sub-categories/{category}', [AdminSubCategoryController::class, 'destroy'])->name('sub-categories.delete');

        //Brands
        Route::get('/brands/create', [AdminBrandsController::class, 'create'])->name('brands.create');
        Route::post('/brands', [AdminBrandsController::class, 'store'])->name('brands.store');
        Route::get('/brands', [AdminBrandsController::class, 'index'])->name('brands.index');
        Route::get('/brands/{brand}/edit', [AdminBrandsController::class, 'edit'])->name('brands.edit');
        Route::put('/brands/{brand}', [AdminBrandsController::class, 'update'])->name('brands.update');


        Route::get('/getSlug', function (Request $request) {
            $slug = '';
            if(!empty($request->title)) {
                $slug = Str::class::slug($request->title);
            }

            return response()->json([
                'status' => true,
                'slug' => $slug
            ]);
        })->name('getSlug');
    });
});